#include "liteipc_simulator.h"
#include "liteipc_adapter.h"
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

int main(void) {
    int ret;
    void *mem;
    bool done = false;

    ret = ConnectSimulator(1000, &mem);
    if (ret != LOS_OK) {
        goto ERROR;
    }

    ret = SetCms(10000);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("server: set CMS\n");

    struct msgData {
        char summary[20];
        SpecialObj command;
        SpecialObj svc;
    };

    IpcMsg msg = {
        .type = MT_REPLY,
        .target = { 0, 0, 0 },
        .flag = LITEIPC_FLAG_DEFAULT,
        .dataSz = sizeof("Hello client!"),
        .data = "Hello client!",
        .spObjNum = 0,
        .offsets = NULL
    };
    IpcContent content = {
        .flag = RECV,
    };

    printf("server: server mode\n");
    do {
        content.flag = RECV;
        ret = LiteIpcMsgHandle(&content);
        printf("server: received message %s\n%s\nservice handle: %"PRIu32"\n", ((struct msgData *)content.inMsg->data)->summary,
            ((struct msgData *)content.inMsg->data)->command.content.ptr.buff,
            ((struct msgData *)content.inMsg->data)->svc.content.svc.handle);
        if (strcmp(((struct msgData *)content.inMsg->data)->summary, "exit") == 0) {
            done = true;
        }

        printf("server: sending message\n");
        content.flag = BUFF_FREE | SEND;
        content.buffToFree = content.inMsg;
        msg.target.handle = content.inMsg->taskID;
        msg.timestamp = content.inMsg->timestamp;
        content.outMsg = &msg;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
    } while(!done);

    printf("server: exiting\n");
    return 0;
ERROR:
    printf("error %d\n", ret);
    return -ret;
}
