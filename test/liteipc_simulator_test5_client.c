#include "liteipc_simulator.h"
#include "liteipc_adapter.h"
#include <stdio.h>

int main(void) {
    int ret;
    void *mem;

    ret = ConnectSimulator(1000, &mem);
    if (ret != LOS_OK) {
        goto ERROR;
    }

    struct msgData {
        char summary[20];
        SpecialObj command;
        SpecialObj svc;
    } msgData = {
        .summary = "one wakeup"
    };
    char commandStr[100] = "Wake me up at 9:00 in the morning.";
    void* dataOffsets[2] = {
        (void*)offsetof(struct msgData, command),
        (void*)offsetof(struct msgData, svc)
    };
    IpcMsg msg = {
        .type = MT_REQUEST,
        .target = { 0, 0, 0 },
        .flag = LITEIPC_FLAG_DEFAULT,
        .dataSz = sizeof(msgData),
        .data = &msgData,
        .spObjNum = 2,
        .offsets = dataOffsets
    };
    IpcContent content = {
        .flag = SEND | RECV,
        .outMsg = &msg
    };

    msgData.command.type = OBJ_PTR;
    msgData.command.content.ptr.buffSz = sizeof(commandStr);
    msgData.command.content.ptr.buff = commandStr;
    msgData.svc.type = OBJ_SVC;
    msgData.svc.content.svc.handle = -1;
    msgData.svc.content.svc.token = 1;
    for(int i = 0; i < 5; i++) {
        content.flag = SEND | RECV;
        printf("client: sending message\n");
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("client: received message %s\n", content.inMsg->data);
        content.flag = BUFF_FREE;
        content.buffToFree = content.inMsg;
        ret = LiteIpcMsgHandle(&content);
    }
    content.flag = SEND;
    sprintf(msgData.summary, "exit");
    ret = LiteIpcMsgHandle(&content);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("client: message sent\n");

    printf("client: exiting\n");
    return 0;
ERROR:
    printf("error %d\n", ret);
    return -ret;
}
