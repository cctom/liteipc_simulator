#include "liteipc_simulator.h"
#include "liteipc_adapter.h"
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <inttypes.h>

struct msgData {
    char summary[20];
    SpecialObj command;
    SpecialObj svc;
};

void *client_task(void *arg) {
    int ret;
    int serviceHandle;

    CmsCmdContent cmsCmd;
    cmsCmd.cmd = CMS_GEN_HANDLE;
    ret = SimulatorInitTask(&cmsCmd.taskID);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    ret = HandleCmsCmd(&cmsCmd);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    serviceHandle = cmsCmd.serviceHandle;
    printf("child: got service handle %"PRIu32" for task %"PRIu32"\n", serviceHandle, cmsCmd.taskID);

    struct msgData msgData = {
        .summary = "one wakeup"
    };
    char commandStr[100] = "Wake me up at 9:00 in the morning.";
    void* dataOffsets[2] = {
        (void*)offsetof(struct msgData, command),
        (void*)offsetof(struct msgData, svc)
    };
    IpcMsg msg = {
        .type = MT_REQUEST,
        .target = { 0, 0, 0 },
        .flag = LITEIPC_FLAG_DEFAULT,
        .dataSz = sizeof(msgData),
        .data = &msgData,
        .spObjNum = 2,
        .offsets = dataOffsets
    };
    IpcContent content = {
        .flag = SEND,
        .outMsg = &msg
    };

    msgData.command.type = OBJ_PTR;
    msgData.command.content.ptr.buffSz = sizeof(commandStr);
    msgData.command.content.ptr.buff = commandStr;
    msgData.svc.type = OBJ_SVC;
    msgData.svc.content.svc.handle = serviceHandle;
    for(int i = 0; i < 5; i++) {
        sprintf(commandStr, "Wake me up at %d:00 in the %s.", (8+i) % 12 + 1, ((8+i) < 12) ? "morning" : "afternoon");
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("child: message sent\n");
    }
    sprintf(msgData.summary, "exit");
    ret = LiteIpcMsgHandle(&content);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("child: message sent\n");

ERROR:
    printf("child: exiting\n");
    return NULL;
}

int main(void) {
    int ret;
    void *mem;
    pthread_t child;
    bool done = false;
    uint32_t taskID;

    ret = ConnectSimulator(1000, &mem);
    if (ret != LOS_OK) {
        goto ERROR;
    }

    ret = SetCms(10000);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("set CMS\n");

    ret = SimulatorInitTask(&taskID);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("got task id %d\n", taskID);

    pthread_create(&child, NULL, client_task, NULL);

    IpcContent content;
    do {
        content.flag = RECV;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("%s\n", ((struct msgData *)content.inMsg->data)->summary);
        printf("%s\n", ((struct msgData *)content.inMsg->data)->command.content.ptr.buff);
        if (strcmp(((struct msgData *)content.inMsg->data)->summary, "exit") == 0) {
            done = true;
        }

        content.flag = BUFF_FREE;
        content.buffToFree = content.inMsg;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
    } while(!done);
    return 0;
ERROR:
    printf("error %d\n", ret);
    return -ret;
}
