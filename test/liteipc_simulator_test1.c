#include "liteipc_simulator.h"
#include "liteipc_adapter.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

int main(void) {
    int ret;
    void *mem;
    bool done = false;

    struct msgData {
        char summary[20];
        SpecialObj command;
        SpecialObj svc;
    } msgData = {
        .summary = "one wakeup"
    };
    char commandStr[100] = "Wake me up at 9:00 in the morning.";
    void* dataOffsets[2] = {
        (void*)offsetof(struct msgData, command),
        (void*)offsetof(struct msgData, svc)
    };
    IpcMsg msg = {
        .type = MT_REQUEST,
        .target = { 0, 0, 0 },
        .flag = LITEIPC_FLAG_DEFAULT,
        .dataSz = sizeof(msgData),
        .data = &msgData,
        .spObjNum = 2,
        .offsets = dataOffsets
    };
    IpcContent content = {
        .flag = SEND,
        .outMsg = &msg
    };

    ret = ConnectSimulator(10000, &mem);
    if (ret != LOS_OK) {
        goto ERROR;
    }

    ret = SetCms(10000);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("set CMS\n");

    content.flag = SEND;
    sprintf(commandStr, "commands pending...");
    msgData.command.type = OBJ_PTR;
    msgData.command.content.ptr.buffSz = sizeof(commandStr);
    msgData.command.content.ptr.buff = commandStr;
    msgData.svc.type = OBJ_SVC;
    msgData.svc.content.svc.handle = 0;
    ret = LiteIpcMsgHandle(&content);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("message sent\n");

    for(int i = 0; i < 5; i++) {
        content.flag = SEND;
        sprintf(commandStr, "Wake me up at %d:00 in the %s.", (8+i) % 12 + 1, ((8+i) < 12) ? "morning" : "afternoon");
        msgData.command.type = OBJ_PTR;
        msgData.command.content.ptr.buffSz = sizeof(commandStr);
        msgData.command.content.ptr.buff = commandStr;
        msgData.svc.type = OBJ_SVC;
        msgData.svc.content.svc.handle = 0;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("message sent\n");
    }

    content.flag = SEND;
    sprintf(msgData.summary, "exit");
    ret = LiteIpcMsgHandle(&content);
    if (ret != LOS_OK) {
        goto ERROR;
    }
    printf("message sent\n");

    do {
        content.flag = RECV;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("received message from tid[%"PRIu32"] pid[%"PRIu32"] uid[%"PRIu32"] gid[%"PRIu32"]\n",
            content.inMsg->taskID, content.inMsg->processID, content.inMsg->userID, content.inMsg->gid);
        printf("%s\n", ((struct msgData *)content.inMsg->data)->summary);
        printf("%s\n", ((struct msgData *)content.inMsg->data)->command.content.ptr.buff);
        if (strcmp(((struct msgData *)content.inMsg->data)->summary, "exit") == 0) {
            done = true;
        }

        content.flag = BUFF_FREE;
        content.buffToFree = (void *)content.inMsg - 1;
        ret = LiteIpcMsgHandle(&content);
        if (ret == -EFAULT) {
            printf("BUFF_FREE pointer checking ok!\n");
        } else {
            goto ERROR;
        }
        content.buffToFree = content.inMsg;
        ret = LiteIpcMsgHandle(&content);
        if (ret != LOS_OK) {
            goto ERROR;
        }
        printf("freed message\n");
    } while(!done);

    return 0;
ERROR:
    printf("error %d\n", ret);
    return -ret;
}
