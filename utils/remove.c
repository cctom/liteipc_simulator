#include "liteipc_simulator_pri.h"
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define INVALID_ID (-1)

SimulatorControlData *g_ipcCtl;

int main(void) {
    int ret;
    int fd;
    char version[32];

    printf("LiteIPC Simulator version %s\n", SIMULATOR_DATA_VERSION);
    fd = shm_open("/liteipc_simulator", O_RDWR, 0);
    if (fd >= 0) {
        if (read(fd, version, 32) != 32) {
            fprintf(stderr, "Invalid simulator data.\nAborting.\n");
            ret = EINVAL;
            goto ERROR;
        }
        if (strcmp(version, SIMULATOR_DATA_VERSION) != 0) {
            fprintf(stderr, "Incompatible simulator data version %s.\nExiting.\n", version);
            ret = EINVAL;
            goto ERROR;
        }
        g_ipcCtl = mmap(NULL, sizeof(SimulatorControlData), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        close(fd);
        if (g_ipcCtl == MAP_FAILED) {
           ret = errno;
           goto ERROR;
        }
        for(int i = 0; i < MAX_TASK_NUM; i++) {
            if (g_ipcCtl->taskCBIpc[i].processID != INVALID_ID) {
                printf("Removing task id %d\n", i);
                sem_destroy(&g_ipcCtl->taskCBIpc[i].messageListSemaphore);
            }
        }
        munmap(g_ipcCtl, sizeof(SimulatorControlData));
        shm_unlink("/liteipc_simulator");
    }
    return 0;

ERROR:
    return ret;
}
