#include "liteipc_simulator_pri.h"
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

SimulatorControlData *g_ipcCtl;

int main(void) {
    int ret = 0;
    int fd;

    fd = shm_open("/liteipc_simulator", O_RDWR | O_CREAT | O_EXCL, 0666);
    if (fd < 0) {
        if (errno == EEXIST) {
            fprintf(stderr, "Error LiteIPC already initialized.\n");
        }
        ret = errno;
        goto ERROR;
    }
    if (ftruncate(fd, sizeof(SimulatorControlData)) < 0) {
        fprintf(stderr, "Error initializing: no memory.\n");
        ret = errno;
        goto ERROR;
    }
    g_ipcCtl = mmap(NULL, sizeof(SimulatorControlData), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (g_ipcCtl == MAP_FAILED) {
        ret = errno;
        goto ERROR;
    }

    if (write(fd, SIMULATOR_DATA_VERSION, sizeof(SIMULATOR_DATA_VERSION)) !=
        sizeof(SIMULATOR_DATA_VERSION)) {
        fprintf(stderr, "Error initializing: no memory.\n");
        ret = ENOMEM;
        goto ERROR;
    }
    close(fd);
    LiteIpcInit(g_ipcCtl);

    printf("LiteIPC Simulator (data version %s): init SimulatorControlData %d bytes\n", g_ipcCtl->version, sizeof(SimulatorControlData));

    munmap(g_ipcCtl, sizeof(SimulatorControlData));

    return 0;
ERROR:
    fprintf(stderr, "Returned error %d.\n", ret);
    return ret;
}
