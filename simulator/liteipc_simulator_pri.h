#include "liteipc.h"
#include <pthread.h>
#include <semaphore.h>

#define LOSCFG_KERNEL_TRACE YES

#define SIMULATOR_DATA_VERSION "0.7.1"

#define LOSCFG_BASE_CORE_TSK_LIMIT 128
#define LOSCFG_BASE_CORE_PROCESS_LIMIT 64
#define MAX_SERVICE_NUM LOSCFG_BASE_CORE_TSK_LIMIT
#define MAX_PROCESS_NUM LOSCFG_BASE_CORE_PROCESS_LIMIT
#define MAX_TASK_NUM    LOSCFG_BASE_CORE_TSK_LIMIT
#define USE_TIMESTAMP YES

typedef enum {
    CANNOT_FREE = 0,
    KERNEL_FREE,
    USER_FREE,
} IpcMemCanFree;

typedef struct IpcMemNode {
    uintptr_t         onextNode;
    IpcMemCanFree     canFree;
} IpcMemNode;

typedef struct {
    void               *memBase;
    int                svID;
    pthread_spinlock_t memLock;
    size_t             memSize;
    uintptr_t          omemListHead;
    uintptr_t          oallocatedMemBase;
} IpcMemPool;

#define REAL_ADDRESS(base,offset) (void *)((uintptr_t)(base) + (uintptr_t)(offset))
#define MEM_REAL_ADDRESS(base,offset) ((IpcMemNode*)REAL_ADDRESS(base,offset))
#define FREE_MEM_BASE(base,mem) ((MEM_REAL_ADDRESS(base,(mem)->omemListHead))->onextNode)
#define IS_MEM_EMPTY(base,mem) ((mem)->oallocatedMemBase == FREE_MEM_BASE(base,mem))

typedef enum {
    HANDLE_NOT_USED,
    HANDLE_REGISTERING,
    HANDLE_REGISTERED
} HandleStatus;

typedef struct {
    int32_t driverVersion;
} IpcVersion;

#define IPC_GET_VERSION     _IOR(IPC_IOC_MAGIC, 5, IpcVersion)

typedef enum {
    LITEIPC_FLAG_DEFAULT = 0, // send and reply
    LITEIPC_FLAG_ONEWAY,      // send message only
} IpcFlag;

typedef struct IpcListNode {
    IpcMsg             msg;
    struct IpcListNode *nextNode;
} IpcListNode;

#define LIST_REAL_ADDRESS(base,offset) ((IpcListNode*)REAL_ADDRESS(base,offset))
#define IS_LIST_EMPTY(taskCBIpc) ((taskCBIpc)->messageListTail == NULL)

typedef struct {
    pid_t        pid;
    IpcMemPool   pool;
    uint32_t     ipcTaskID;
} ProcessCBIpc;

typedef struct {
    uint32_t           processID;
    pthread_spinlock_t messageListLock;
    uintptr_t          omessageListHead;
    IpcListNode        *messageListTail;
    sem_t              messageListSemaphore;
    uint32_t           serviceHandle;
} TaskCBIpc;

typedef struct SimulatorControlData {
    char               version[32];

    pthread_spinlock_t serviceHandleStackLock;
    uint32_t           serviceHandleStack[MAX_SERVICE_NUM];
    int                serviceHandleStackTop;
    HandleStatus       serviceStatus[MAX_SERVICE_NUM];
    uint32_t           serviceTaskID[MAX_SERVICE_NUM];

    pthread_spinlock_t processIDStackLock;
    uint32_t           processIDStack[MAX_PROCESS_NUM];
    int                processIDStackTop;
    ProcessCBIpc       processCBIpc[MAX_PROCESS_NUM];

    pthread_spinlock_t taskIDStackLock;
    uint32_t           taskIDStack[MAX_TASK_NUM];
    int                taskIDStackTop;
    TaskCBIpc          taskCBIpc[MAX_TASK_NUM];
    bool               accessMap[MAX_TASK_NUM][MAX_PROCESS_NUM];

    uintptr_t          maxMsgSize;
} SimulatorControlData;

int LiteIpcInit(SimulatorControlData *controlData);
