#include "liteipc.h"
#include <sys/types.h>

#define LOS_OK  0
#define LOS_NOK 1

int ConnectSimulator(size_t size, void **memPtr);
int SetCms(uintptr_t maxMsgSize);
int HandleCmsCmd(CmsCmdContent *content);
int SetIpcTask(void);
int LiteIpcMsgHandle(IpcContent *content);

int SimulatorInitTask(uint32_t *taskID);
pid_t SimulatorGetSysPid(uint32_t processID);
