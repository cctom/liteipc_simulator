#include "liteipc_simulator_pri.h"
#include "liteipc_simulator.h"
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <errno.h>
#include <semaphore.h>
#include <time.h>
#include <sched.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <signal.h>

#define IPC_IO_DATA_MAX 8192UL
#define IPC_MSG_DATA_SZ_MAX (IPC_IO_DATA_MAX * sizeof(SpecialObj) / (sizeof(SpecialObj) + sizeof(size_t)))
#define IPC_MSG_OBJECT_NUM_MAX (IPC_MSG_DATA_SZ_MAX / sizeof(SpecialObj))

#define INVALID_ID (-1)

#define LITEIPC_TIMEOUT_S  5
#define LITEIPC_TIMEOUT_NS 5000000000ULL

#define MAJOR_VERSION (2)
#define MINOR_VERSION (0)
#define DRIVER_VERSION (MAJOR_VERSION | MINOR_VERSION << 16)

static SimulatorControlData *g_ipc = NULL;
static pthread_mutex_t      g_processLock = PTHREAD_MUTEX_INITIALIZER;
static uint32_t             g_processID = INVALID_ID;
static void                 *g_processShm = NULL;
static pthread_key_t        g_taskID;
static pthread_mutex_t      g_serviceHandleMapMux = PTHREAD_MUTEX_INITIALIZER;

#define OFFSET_ADDR(mem) ((uintptr_t)(mem) - (uintptr_t)g_processShm)

#define SET_MYTASKID(id) pthread_setspecific(g_taskID, (void *)((id) + 1))
#define GET_MYTASKID ((uint32_t)(uintptr_t)pthread_getspecific(g_taskID) - 1)

#define IS_PROCESS_ALIVE(processID) (((processID) != INVALID_ID) && \
    (g_ipc->processCBIpc[(processID)].pool.memBase != NULL) &&      \
    ((kill(g_ipc->processCBIpc[(processID)].pid, 0) == 0) || (errno != ESRCH)))
#define IS_TASK_ALIVE(taskID) (((taskID) != INVALID_ID) && IS_PROCESS_ALIVE(g_ipc->taskCBIpc[(taskID)].processID))

#define USER_ENTRY_PROCEDURE(myTaskID)                                          \
    {                                                                           \
        if (g_processID == INVALID_ID || g_ipc == NULL) {                       \
            PRINT_ERR("Ipc pool not init, need to connect simulator first!\n"); \
            return -ENOMEM;                                                     \
        }                                                                       \
        myTaskID = GET_MYTASKID;                                                \
        if (myTaskID == INVALID_ID) {                                           \
            GetTaskID(&myTaskID);                                               \
            if (myTaskID == INVALID_ID) {                                       \
                return -EAGAIN;                                                 \
            }                                                                   \
        }                                                                       \
    }

#define INVALID_SERVICE_HANDLE MAX_SERVICE_NUM

static int LiteIpcNodeFree(void *memBase, uint32_t processID, uintptr_t obuf);
static int GarbageCollectProcesses(void);
static int GetProcessID(size_t size, void **memPtr);
static int SendDeathMsg(uint32_t processID, uint32_t serviceHandle);
static int LiteIpcWrite(IpcContent *content);

#if (LOSCFG_KERNEL_TRACE == YES)

typedef enum {
    WRITE,
    WRITE_DROP,
    TRY_READ,
    READ,
    READ_DROP,
    READ_TIMEOUT,
    KILL,
    OPERATION_NUM
} IpcOpertion;

static void IpcTrace(IpcMsg *msg, IpcOpertion operation, int ipcStatus, int msgType)
{
}

static void IpcBacktrace(void)
{
}
#endif

#define PRINT_ERR printf
#define PRINT_DEBUG printf

static int GenerateServiceHandle(uint32_t *serviceHandle) {
    pthread_spin_lock(&g_ipc->serviceHandleStackLock);
    if (g_ipc->serviceHandleStackTop >= MAX_SERVICE_NUM) {
        pthread_spin_unlock(&g_ipc->serviceHandleStackLock);
        GarbageCollectProcesses();
        pthread_spin_lock(&g_ipc->serviceHandleStackLock);
        if (g_ipc->serviceHandleStackTop >= MAX_SERVICE_NUM) {
            pthread_spin_unlock(&g_ipc->serviceHandleStackLock);
            return -EAGAIN;
        }
    }
    *serviceHandle = g_ipc->serviceHandleStack[g_ipc->serviceHandleStackTop];
    g_ipc->serviceHandleStackTop++;
    pthread_spin_unlock(&g_ipc->serviceHandleStackLock);
    return LOS_OK;
}

static int RemoveServiceHandle(uint32_t serviceHandle) {
    pthread_spin_lock(&g_ipc->serviceHandleStackLock);
    if (g_ipc->serviceHandleStackTop == 0) {
        pthread_spin_unlock(&g_ipc->serviceHandleStackLock);
        return -EINVAL;
    }
    g_ipc->serviceHandleStackTop--;
    g_ipc->serviceHandleStack[g_ipc->serviceHandleStackTop] = serviceHandle;
    pthread_spin_unlock(&g_ipc->serviceHandleStackLock);
    return LOS_OK;
}

static int GenerateProcessID(uint32_t *processID) {
    pthread_spin_lock(&g_ipc->processIDStackLock);
    if (g_ipc->processIDStackTop >= MAX_PROCESS_NUM) {
        pthread_spin_unlock(&g_ipc->processIDStackLock);
        GarbageCollectProcesses();
        pthread_spin_lock(&g_ipc->processIDStackLock);
        if (g_ipc->processIDStackTop >= MAX_PROCESS_NUM) {
            pthread_spin_unlock(&g_ipc->processIDStackLock);
            return -EAGAIN;
        }
    }
    *processID = g_ipc->processIDStack[g_ipc->processIDStackTop];
    g_ipc->processIDStackTop++;
    pthread_spin_unlock(&g_ipc->processIDStackLock);
    return LOS_OK;
}

static int RemoveProcessID(uint32_t processID) {
    pthread_spin_lock(&g_ipc->processIDStackLock);
    if (g_ipc->processIDStackTop == 0) {
        pthread_spin_unlock(&g_ipc->processIDStackLock);
        return -EINVAL;
    }
    g_ipc->processIDStackTop--;
    g_ipc->processIDStack[g_ipc->processIDStackTop] = processID;
    pthread_spin_unlock(&g_ipc->processIDStackLock);
    return LOS_OK;
}

static int GenerateTaskID(uint32_t *taskID) {
    pthread_spin_lock(&g_ipc->taskIDStackLock);
    if (g_ipc->taskIDStackTop >= MAX_TASK_NUM) {
        pthread_spin_unlock(&g_ipc->taskIDStackLock);
        GarbageCollectProcesses();
        pthread_spin_lock(&g_ipc->taskIDStackLock);
        if (g_ipc->taskIDStackTop >= MAX_TASK_NUM) {
            pthread_spin_unlock(&g_ipc->taskIDStackLock);
            return -EAGAIN;
        }
    }
    *taskID = g_ipc->taskIDStack[g_ipc->taskIDStackTop];
    g_ipc->taskIDStackTop++;
    pthread_spin_unlock(&g_ipc->taskIDStackLock);
    return LOS_OK;
}

static int RemoveTaskID(uint32_t taskID) {
    pthread_spin_lock(&g_ipc->taskIDStackLock);
    if (g_ipc->taskIDStackTop == 0) {
        pthread_spin_unlock(&g_ipc->taskIDStackLock);
        return -EINVAL;
    }
    g_ipc->taskIDStackTop--;
    g_ipc->taskIDStack[g_ipc->taskIDStackTop] = taskID;
    pthread_spin_unlock(&g_ipc->taskIDStackLock);
    return LOS_OK;
}

int LiteIpcInit(SimulatorControlData *controlData) {
    if (controlData == NULL) {
        PRINT_ERR("Invalid control data pointer.\n");
        return -EINVAL;
    }
    controlData->version[0] = 0;
    if (pthread_spin_init(&controlData->serviceHandleStackLock, PTHREAD_PROCESS_SHARED) != 0) {
        return -EINVAL;
    }
    if (pthread_spin_init(&controlData->processIDStackLock, PTHREAD_PROCESS_SHARED) != 0) {
        return -EINVAL;
    }
    if (pthread_spin_init(&controlData->taskIDStackLock, PTHREAD_PROCESS_SHARED) != 0) {
        return -EINVAL;
    }
    for(int i = 0; i < MAX_SERVICE_NUM; i++) {
        controlData->serviceHandleStack[i] = i;
        controlData->serviceStatus[i] = HANDLE_NOT_USED;
        controlData->serviceTaskID[i] = INVALID_ID;
    }
    // reserve service handle 0 for the CMS
    controlData->serviceHandleStackTop = 1;
    for(int i = 0; i < MAX_PROCESS_NUM; i++) {
        controlData->processIDStack[i] = i;
        controlData->processCBIpc[i].pool.memBase = NULL;
        controlData->processCBIpc[i].pool.svID = 0;
        controlData->processCBIpc[i].pool.memSize = 0;
        controlData->processCBIpc[i].pool.omemListHead = 0;
        controlData->processCBIpc[i].pool.oallocatedMemBase = 0;
        controlData->processCBIpc[i].ipcTaskID = INVALID_ID;
    }
    controlData->processIDStackTop = 0;
    for(int i = 0; i < MAX_TASK_NUM; i++) {
        controlData->taskIDStack[i] = i;
        controlData->taskCBIpc[i].processID = INVALID_ID;
        controlData->taskCBIpc[i].omessageListHead = 0;
        controlData->taskCBIpc[i].messageListTail = NULL;
        controlData->taskCBIpc[i].serviceHandle = INVALID_SERVICE_HANDLE;
    }
    for(int i = 0; i < MAX_TASK_NUM; i++) {
        for(int j = 0; j < MAX_PROCESS_NUM; j++) {
            controlData->accessMap[i][j] = false;
        }
    }
    controlData->taskIDStackTop = 0;
    strcpy(controlData->version, SIMULATOR_DATA_VERSION);
    return LOS_OK;    
}

int ConnectSimulator(size_t size, void **memPtr) {
    int ret;
    int fd;
    char version[32];
    pthread_mutex_lock(&g_processLock);
    if (g_ipc != NULL) {
        ret = -EEXIST;
        goto ERROR_NODATA;
    }
    fd = shm_open("/liteipc_simulator", O_RDWR, 0666);
    if (fd < 0) {
        if (errno == ENOENT) {
            PRINT_ERR("Error LiteIPC not initialized.\n");
        }
        ret = -errno;
        goto ERROR_NODATA;
    }
    if (read(fd, version, 32) != 32) {
        PRINT_ERR("Invalid simulator data.\nAborting.\n");
        ret = -ENOENT;
        goto ERROR;
    }
    if (strcmp(version, SIMULATOR_DATA_VERSION) != 0) {
        PRINT_ERR("Incompatible simulator data version %s.\nExiting.\n", version);
        ret = -ENOENT;
        goto ERROR;
    }
    g_ipc = mmap(NULL, sizeof(SimulatorControlData), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (g_ipc == MAP_FAILED) {
        g_ipc = NULL;
        ret = -errno;
        goto ERROR;
    }
    ret = GetProcessID(size, memPtr);
    if (ret != LOS_OK) {
        munmap(g_ipc, sizeof(SimulatorControlData));
        g_ipc = NULL;
        goto ERROR;
    }
    ret = LOS_OK;
ERROR:
    close(fd);
ERROR_NODATA:
    pthread_mutex_unlock(&g_processLock);
    return ret;
}

static int GetProcessID(size_t size, void **memPtr) {
    uint32_t id;
    int shmId;
    void *shmPtr;

    if (g_processID != INVALID_ID) {
        return -EEXIST;
    }
    if (GenerateProcessID(&id) != LOS_OK) {
        return -EAGAIN;
    }
    g_ipc->processCBIpc[id].pid = getpid();
    if (pthread_key_create(&g_taskID, NULL) != 0) {
        RemoveProcessID(id);
        return -EAGAIN;
    }
    if (pthread_spin_init(&g_ipc->processCBIpc[id].pool.memLock, PTHREAD_PROCESS_SHARED) != 0) {
        pthread_key_delete(g_taskID);
        RemoveProcessID(id);
        return -EINVAL;
    }
    shmId = shmget(IPC_PRIVATE, size, 0666);
    if (shmId == -1) {
        pthread_spin_destroy(&g_ipc->processCBIpc[id].pool.memLock);
        pthread_key_delete(g_taskID);
        RemoveProcessID(id);
        return -ENOMEM;
    }
    shmPtr = shmat(shmId, NULL, 0);
    if (shmPtr == (void*)-1) {
        pthread_spin_destroy(&g_ipc->processCBIpc[id].pool.memLock);
        pthread_key_delete(g_taskID);
        RemoveProcessID(id);
        return -ENOMEM;
    }
    shmctl(shmId, IPC_RMID, NULL);
    *memPtr = shmPtr;
    g_ipc->processCBIpc[id].pool.svID = shmId;
    g_ipc->processCBIpc[id].pool.omemListHead = 0;
    FREE_MEM_BASE(shmPtr, &g_ipc->processCBIpc[id].pool) = 0;
    g_ipc->processCBIpc[id].pool.oallocatedMemBase = 0;
    g_ipc->processCBIpc[id].pool.memSize = size;
    g_ipc->processCBIpc[id].pool.memBase = shmPtr;
    g_processShm = shmPtr;
    g_processID = id;
    return LOS_OK;    
}

static int GetTaskID(uint32_t *taskID) {
    uint32_t id;

    id = GET_MYTASKID;
    if (id != INVALID_ID) {
        *taskID = id;
        return LOS_OK;
    }
    if (GenerateTaskID(&id) != LOS_OK) {
        return -EAGAIN;
    }
    g_ipc->taskCBIpc[id].processID = g_processID;
    if (pthread_spin_init(&g_ipc->taskCBIpc[id].messageListLock, PTHREAD_PROCESS_SHARED) != 0) {
        RemoveTaskID(id);
        return -EINVAL;
    }
    g_ipc->taskCBIpc[id].omessageListHead = 0;
    g_ipc->taskCBIpc[id].messageListTail = NULL;
    if (sem_init(&g_ipc->taskCBIpc[id].messageListSemaphore, 1, 0) == -1) {
        pthread_spin_destroy(&g_ipc->taskCBIpc[id].messageListLock);
        RemoveTaskID(id);
        return -errno;
    }
    g_ipc->taskCBIpc[id].serviceHandle = INVALID_SERVICE_HANDLE;
    for(int j = 0; j < MAX_PROCESS_NUM; j++) {
        g_ipc->accessMap[id][j] = false;
    }
    SET_MYTASKID((uintptr_t)id);
    *taskID = id;
    return LOS_OK;
}

static int GetServiceHandle(uint32_t taskID, HandleStatus status, uint32_t *serviceHandle) {
    uint32_t handle;

    if (g_ipc->taskCBIpc[taskID].serviceHandle != INVALID_SERVICE_HANDLE) {
        *serviceHandle = g_ipc->taskCBIpc[taskID].serviceHandle;
        return LOS_OK;
    }
    if (GenerateServiceHandle(&handle) != LOS_OK) {
        return -EAGAIN;
    }
    g_ipc->serviceTaskID[handle] = taskID;
    g_ipc->serviceStatus[handle] = status;
    g_ipc->taskCBIpc[taskID].serviceHandle = handle;
    *serviceHandle = handle;
    return LOS_OK;
}

static int GarbageCollectServices(void) {
    if ((g_ipc->serviceStatus[0] != HANDLE_NOT_USED) && (g_ipc->taskCBIpc[g_ipc->serviceTaskID[0]].processID == INVALID_ID)) {
        g_ipc->serviceStatus[0] = HANDLE_NOT_USED;
        g_ipc->taskCBIpc[g_ipc->serviceTaskID[0]].serviceHandle = INVALID_SERVICE_HANDLE;
        g_ipc->serviceTaskID[0] = INVALID_ID;
    }
    for(int i = 1; i < MAX_SERVICE_NUM; i++) {
        if ((g_ipc->serviceStatus[i] != HANDLE_NOT_USED) && (g_ipc->taskCBIpc[g_ipc->serviceTaskID[i]].processID == INVALID_ID)) {
            g_ipc->serviceStatus[i] = HANDLE_NOT_USED;
            for(int j = 0; j < MAX_PROCESS_NUM; j++) {
                if (g_ipc->accessMap[g_ipc->serviceTaskID[i]][j]) {
                    (void)SendDeathMsg(j, i);
                }
            }
            g_ipc->taskCBIpc[g_ipc->serviceTaskID[i]].serviceHandle = INVALID_SERVICE_HANDLE;
            g_ipc->serviceTaskID[i] = INVALID_ID;
            RemoveServiceHandle(i);
        }
    }
    return LOS_OK;
}

static int GarbageCollectTasks(void) {
    int collectedTaskStack[MAX_TASK_NUM];
    int collectedTaskStackTop = 0;
    for(int i = 0; i < MAX_TASK_NUM; i++) {
        // INCOMPLETE: threads can end when their process is still running
        if ((g_ipc->taskCBIpc[i].processID != INVALID_ID) && (g_ipc->processCBIpc[g_ipc->taskCBIpc[i].processID].pool.memBase == NULL)) {
            g_ipc->taskCBIpc[i].processID = INVALID_ID;
            pthread_spin_destroy(&g_ipc->taskCBIpc[i].messageListLock);
            sem_destroy(&g_ipc->taskCBIpc[i].messageListSemaphore);
            collectedTaskStack[collectedTaskStackTop] = i;
            collectedTaskStackTop++;
        }
    }
    GarbageCollectServices();
    while(collectedTaskStackTop > 0) {
        collectedTaskStackTop--;
        RemoveTaskID(collectedTaskStack[collectedTaskStackTop]);
    }
    return LOS_OK;
}

static int GarbageCollectProcesses(void) {
    int collectedProcessStack[MAX_PROCESS_NUM];
    int collectedProcessStackTop = 0;
    for(int i = 0; i < MAX_PROCESS_NUM; i++) {
        if ((g_ipc->processCBIpc[i].pool.memBase != NULL) && (kill(g_ipc->processCBIpc[i].pid, 0) == -1) && (errno == ESRCH)) {
            g_ipc->processCBIpc[i].pool.memBase = NULL;
            pthread_spin_destroy(&g_ipc->processCBIpc[i].pool.memLock);
            collectedProcessStack[collectedProcessStackTop] = i;
            collectedProcessStackTop++;
        }
    }
    GarbageCollectTasks();
    while(collectedProcessStackTop > 0) {
        collectedProcessStackTop--;
        RemoveProcessID(collectedProcessStack[collectedProcessStackTop]);
    }
    return LOS_OK;
}

/* Only when kernenl no longer access ipc node content, can user free the ipc node */
static void EnableIpcNodeFreeByUser(uint32_t processID, void *buf)
{
    IpcMemPool *pool = &g_ipc->processCBIpc[processID].pool;
    IpcMemNode *bufNode = (IpcMemNode *)((uintptr_t)buf - sizeof(IpcMemNode));
    pthread_spin_lock(&pool->memLock);
    if (bufNode->canFree == CANNOT_FREE) {
        bufNode->canFree = USER_FREE;
    }
    pthread_spin_unlock(&pool->memLock);
}

static int LiteIpcNodeAlloc(void *memBase, uint32_t processID, int size, uintptr_t *onewNode) {
    int nodeSize = sizeof(IpcMemNode) + size;
    IpcMemPool *pool = &g_ipc->processCBIpc[processID].pool;
    pthread_spin_lock(&pool->memLock);
    if (FREE_MEM_BASE(memBase, pool) >= pool->oallocatedMemBase) {
        if (FREE_MEM_BASE(memBase, pool) + nodeSize > pool->memSize) {
            if ((nodeSize < pool->oallocatedMemBase)) {
                MEM_REAL_ADDRESS(memBase, pool->omemListHead)->onextNode = 0;
            } else {
                pthread_spin_unlock(&pool->memLock);
                return -ENOMEM;
            }
        }
    } else {
        // quick-easy memory management, don't let the head overrun the tail
        if (FREE_MEM_BASE(memBase, pool) + nodeSize >= pool->oallocatedMemBase) {
            pthread_spin_unlock(&pool->memLock);
            return -ENOMEM;
        }
    }
    pool->omemListHead = MEM_REAL_ADDRESS(memBase, pool->omemListHead)->onextNode;
    MEM_REAL_ADDRESS(memBase, pool->omemListHead)->canFree = CANNOT_FREE;
    FREE_MEM_BASE(memBase, pool) = pool->omemListHead + nodeSize;
    *onewNode = pool->omemListHead + sizeof(IpcMemNode);
    pthread_spin_unlock(&pool->memLock);
    return LOS_OK;
}

static int LiteIpcNodeFree(void *memBase, uint32_t processID, uintptr_t obuf) {
    IpcMemPool *pool = &g_ipc->processCBIpc[processID].pool;
    uintptr_t onode;
    onode = obuf - sizeof(IpcMemNode);
    pthread_spin_lock(&pool->memLock);
    if (onode == pool->oallocatedMemBase) {
        pool->oallocatedMemBase = MEM_REAL_ADDRESS(memBase, pool->oallocatedMemBase)->onextNode;
        while(!IS_MEM_EMPTY(memBase, pool) &&
            (MEM_REAL_ADDRESS(memBase, pool->oallocatedMemBase)->canFree) == KERNEL_FREE) {
            pool->oallocatedMemBase = MEM_REAL_ADDRESS(memBase, pool->oallocatedMemBase)->onextNode;
        }
        if (IS_MEM_EMPTY(memBase, pool)) {
            pool->omemListHead = 0;
            FREE_MEM_BASE(memBase, pool) = 0;
            pool->oallocatedMemBase = 0;
        }
    } else {
        MEM_REAL_ADDRESS(memBase, onode)->canFree = KERNEL_FREE;
    }
    pthread_spin_unlock(&pool->memLock);
    return LOS_OK;
}

static bool IsIpcNode(IpcMemPool *pool, const uintptr_t obuf)
{
    uintptr_t bufNode = obuf - sizeof(IpcMemNode);
    uintptr_t node;
    pthread_spin_lock(&pool->memLock);
    for(node = pool->oallocatedMemBase; node != FREE_MEM_BASE(g_processShm, pool); node = MEM_REAL_ADDRESS(g_processShm, node)->onextNode) {
        if (node == bufNode) {
            pthread_spin_unlock(&pool->memLock);
            return MEM_REAL_ADDRESS(g_processShm, node)->canFree == USER_FREE;
        }
    }
    pthread_spin_unlock(&pool->memLock);
    return false;
}

static int CheckUsedBuffer(const void *node, uintptr_t *ooutPtr)
{
    uintptr_t optr;
    IpcMemPool *pool = &g_ipc->processCBIpc[g_processID].pool;
    if ((node == NULL) || ((uintptr_t)node < (uintptr_t)pool->memBase) ||
        ((uintptr_t)node >= (uintptr_t)pool->memBase + pool->memSize)) {
        return -EINVAL;
    }
    optr = OFFSET_ADDR(node);
    if (IsIpcNode(pool, optr) != true) {
        return -EFAULT;
    }
    *ooutPtr = optr;
    return LOS_OK;
}

static uint32_t GetTid(uint32_t serviceHandle, uint32_t *taskID)
{
    if (serviceHandle >= MAX_SERVICE_NUM) {
        return -EINVAL;
    }
    pthread_mutex_lock(&g_serviceHandleMapMux);
    if (g_ipc->serviceStatus[serviceHandle] == HANDLE_REGISTERED) {
        *taskID = g_ipc->serviceTaskID[serviceHandle];
        pthread_mutex_unlock(&g_serviceHandleMapMux);
        return LOS_OK;
    }
    pthread_mutex_unlock(&g_serviceHandleMapMux);
    return -EINVAL;
}

static void RefreshServiceHandle(uint32_t serviceHandle, int result)
{
    pthread_mutex_lock(&g_serviceHandleMapMux);
    if ((result == LOS_OK) && (g_ipc->serviceStatus[serviceHandle] == HANDLE_REGISTERING)) {
        g_ipc->serviceStatus[serviceHandle] = HANDLE_REGISTERED;
    } else {
        g_ipc->serviceStatus[serviceHandle] = HANDLE_NOT_USED;
        if (g_ipc->serviceTaskID[serviceHandle] != INVALID_ID) {
            g_ipc->taskCBIpc[g_ipc->serviceTaskID[serviceHandle]].serviceHandle = INVALID_SERVICE_HANDLE;
            g_ipc->serviceTaskID[serviceHandle] = INVALID_ID;
        }
    }
    pthread_mutex_unlock(&g_serviceHandleMapMux);
}

static int AddServiceAccess(uint32_t taskID, uint32_t serviceHandle) {
    uint32_t serviceTid;
    int ret = GetTid(serviceHandle, &serviceTid);
    if (ret != LOS_OK) {
        PRINT_ERR("AddServiceAccess GetTid failed\n");
        return ret;
    }
    g_ipc->accessMap[serviceTid][g_ipc->taskCBIpc[taskID].processID] = true;
    return LOS_OK;    
}

static bool HasServiceAccess(uint32_t serviceHandle) {
    uint32_t serviceTid;
    int ret;
    if (serviceHandle >= MAX_SERVICE_NUM) {
        return false;
    }
    if (serviceHandle == 0) {
        return true;
    }
    ret = GetTid(serviceHandle, &serviceTid);
    if (ret != LOS_OK) {
        PRINT_ERR("HasServiceAccess GetTid failed\n");
        return false;
    }
    if (g_ipc->taskCBIpc[serviceTid].processID == g_processID) {
        return true;
    }
    return g_ipc->accessMap[serviceTid][g_processID];
}

int SetIpcTask(void)
{
    uint32_t myTaskID;
    USER_ENTRY_PROCEDURE(myTaskID);

    if (g_ipc->processCBIpc[g_processID].ipcTaskID == INVALID_ID) {
        g_ipc->processCBIpc[g_processID].ipcTaskID = myTaskID;
        return myTaskID;
    }
    PRINT_ERR("curprocess %d IpcTask already set!\n", g_processID);
    return -EINVAL;
}

static int GetIpcTaskID(uint32_t processID, uint32_t *ipcTaskID)
{
    if (g_ipc->processCBIpc[processID].ipcTaskID == INVALID_ID) {
        return LOS_NOK;
    }
    *ipcTaskID = g_ipc->processCBIpc[processID].ipcTaskID;
    return LOS_OK;
}

static int SendDeathMsg(uint32_t processID, uint32_t serviceHandle)
{
    uint32_t ipcTaskID;
    int ret;
    IpcContent content;
    IpcMsg msg;

    ret = GetIpcTaskID(processID, &ipcTaskID);
    if (ret != LOS_OK) {
        return -EINVAL;
    }
    content.flag = SEND;
    content.outMsg = &msg;
    memset(content.outMsg, 0, sizeof(IpcMsg));
    content.outMsg->type = MT_DEATH_NOTIFY;
    content.outMsg->target.handle = ipcTaskID;
    content.outMsg->target.token = serviceHandle;
    content.outMsg->code = 0;
    return LiteIpcWrite(&content);
}

int SetCms(uintptr_t maxMsgSize) {
    uint32_t myTaskID;
    USER_ENTRY_PROCEDURE(myTaskID);

    if (maxMsgSize < sizeof(IpcMsg)) {
        return -EINVAL;
    }
    pthread_mutex_lock(&g_serviceHandleMapMux);
    // reset CMS if we find the process is dead, but not actually LiteIPC-consistent
    if ((g_ipc->serviceStatus[0] != HANDLE_NOT_USED) &&
        ((kill(g_ipc->processCBIpc[g_ipc->taskCBIpc[g_ipc->serviceTaskID[0]].processID].pid, 0) == -1) && (errno == ESRCH))) {
        GarbageCollectProcesses();
    }
    if (g_ipc->serviceStatus[0] == HANDLE_NOT_USED) {
        g_ipc->serviceStatus[0] = HANDLE_REGISTERED;
        g_ipc->serviceTaskID[0] = myTaskID;
        g_ipc->taskCBIpc[myTaskID].serviceHandle = 0;
        g_ipc->maxMsgSize = maxMsgSize;
        pthread_mutex_unlock(&g_serviceHandleMapMux);
        return LOS_OK;
    }
    pthread_mutex_unlock(&g_serviceHandleMapMux);
    return -EEXIST;
}

static bool IsCmsSet(void) {
    return g_ipc->serviceStatus[0] == HANDLE_REGISTERED;
}

static bool IsCmsTask(uint32_t taskID) {
    return IsCmsSet() ? (g_ipc->taskCBIpc[taskID].processID ==
        g_ipc->taskCBIpc[g_ipc->serviceTaskID[0]].processID) : false;
}

int HandleFd(SpecialObj *obj, bool isRollback) {
    /* now fd is not Isolated between processes, do nothing */
    return LOS_OK;
}

static int HandlePtr(void *memBase, uintptr_t *obuf, uint32_t processID, SpecialObj *obj, bool isRollback) {
    if ((obj->content.ptr.buff == NULL) || (obj->content.ptr.buffSz == 0)) {
        return -EINVAL;
    }
    if (isRollback == false) {
        memcpy(REAL_ADDRESS(memBase, *obuf), obj->content.ptr.buff, obj->content.ptr.buffSz);
        obj->content.ptr.buff = REAL_ADDRESS(g_ipc->processCBIpc[processID].pool.memBase, *obuf);
        *obuf += obj->content.ptr.buffSz;
    }
    return LOS_OK;
}

static int HandleSvc(uint32_t dstTid, SpecialObj *obj, bool isRollback) {
    int ret;
    uint32_t taskID = 0;
    if (isRollback == false) {
        if (obj->content.svc.handle == -1) {
            if (obj->content.svc.token != 1) {
                PRINT_ERR("Liteipc HandleSvc wrong svc token\n");
                return -EINVAL;
            }
            uint32_t selfTid = GET_MYTASKID;
            uint32_t serviceHandle = 0;
            ret = GetServiceHandle(selfTid, HANDLE_REGISTERED, &serviceHandle);
            if (ret != LOS_OK) {
                PRINT_ERR("Liteipc GenerateServiceHandle failed.\n");
                return ret;
            }
            obj->content.svc.handle = serviceHandle;
            AddServiceAccess(dstTid, serviceHandle);
        }
        ret = GetTid(obj->content.svc.handle, &taskID);
        if (ret != LOS_OK) {
            PRINT_ERR("HandleSvc GetTid failed\n");
            return ret;
        }
        if (IS_TASK_ALIVE(taskID) == false) {
            PRINT_ERR("HandleSvc wrong svctid\n");
            return -EINVAL;
        }
        if (HasServiceAccess(obj->content.svc.handle) == false) {
            PRINT_ERR("%s, %d, svchandle:%d, tid:%d\n", __FUNCTION__, __LINE__, obj->content.svc.handle, GET_MYTASKID);
            return -EACCES;
        }
        AddServiceAccess(dstTid, obj->content.svc.handle);
    }
    return LOS_OK;
}

static int HandleObj(void *memBase, uintptr_t *obuf, uint32_t dstTid, SpecialObj *obj, bool isRollback)
{
    int ret;
    switch (obj->type) {
        case OBJ_FD:
            ret = HandleFd(obj, isRollback);
            break;
        case OBJ_PTR:
            ret = HandlePtr(memBase, obuf, g_ipc->taskCBIpc[dstTid].processID, obj, isRollback);
            break;
        case OBJ_SVC:
            ret = HandleSvc(dstTid, (SpecialObj *)obj, isRollback);
            break;
        default:
            ret = -EINVAL;
            break;
    }
    return ret;
}

static int HandleSpecialObjects(void *memBase, uintptr_t obuf, uint32_t dstTid, uintptr_t onode, bool isRollback)
{
    IpcMsg *msg = &(LIST_REAL_ADDRESS(memBase, onode))->msg;

    int ret = LOS_OK;
    int i;
    SpecialObj *obj = NULL;
    uintptr_t *offset = (uintptr_t *)REAL_ADDRESS(memBase, msg->offsets);
    if (isRollback) {
        i = msg->spObjNum;
        goto EXIT;
    }
    for (i = 0; i < msg->spObjNum; i++) {
        if (offset[i] > msg->dataSz - sizeof(SpecialObj)) {
            ret = -EINVAL;
            goto EXIT;
        }
        if ((i > 0) && (offset[i] < offset[i - 1] + sizeof(SpecialObj))) {
            ret = -EINVAL;
            goto EXIT;
        }
        obj = (SpecialObj *)((uintptr_t)REAL_ADDRESS(memBase, msg->data) + offset[i]);
        if (obj == NULL) {
            ret = -EINVAL;
            goto EXIT;
        }
        ret = HandleObj(memBase, &obuf, dstTid, obj, false);
        if (ret != LOS_OK) {
            goto EXIT;
        }
    }
    return LOS_OK;
EXIT:
    for (i--; i >= 0; i--) {
        obj = (SpecialObj *)((uintptr_t)REAL_ADDRESS(memBase, msg->data) + offset[i]);
        (void)HandleObj(memBase, &obuf, dstTid, obj, true);
    }
    return ret;
}

static int GetMsgSize(IpcMsg *msg, uint64_t *size)
{
    uint64_t totalSize;
    int i;
    uintptr_t *offset = (uintptr_t *)(msg->offsets);
    SpecialObj *obj = NULL;
    totalSize = sizeof(IpcMsg) + msg->dataSz + msg->spObjNum * sizeof(SpecialObj *);
    for (i = 0; i < msg->spObjNum; i++) {
        if (offset[i] > msg->dataSz - sizeof(SpecialObj)) {
            return -EINVAL;
        }
        if ((i > 0) && (offset[i] < offset[i - 1] + sizeof(SpecialObj))) {
            return -EINVAL;
        }
        obj = (SpecialObj *)((uintptr_t)msg->data + offset[i]);
        if (obj == NULL) {
            return -EINVAL;
        }
        if (obj->type == OBJ_PTR) {
            totalSize += obj->content.ptr.buffSz;
        }
    }
    *size = totalSize;
    return LOS_OK;
}

static int CopyDataFromUser(void *memBase, uintptr_t onode, const IpcMsg *msg)
{
    IpcListNode *node = LIST_REAL_ADDRESS(memBase, onode);

    memcpy(&node->msg, (const void *)msg, sizeof(IpcMsg));

    if (msg->dataSz) {
        node->msg.data = (void *)(onode + sizeof(IpcListNode));
        memcpy(REAL_ADDRESS(memBase, node->msg.data), msg->data, msg->dataSz);
    } else {
        node->msg.data = NULL;
    }

    if (msg->spObjNum) {
        node->msg.offsets = (void *)(onode + sizeof(IpcListNode) + msg->dataSz);
        memcpy(REAL_ADDRESS(memBase, node->msg.offsets), msg->offsets, msg->spObjNum * sizeof(SpecialObj *));
    } else {
        node->msg.offsets = NULL;
    }
    node->msg.taskID = GET_MYTASKID;
    node->msg.processID = g_processID;
#ifdef LOSCFG_SECURITY_CAPABILITY
    node->msg.userID = getuid();
    node->msg.gid = getgid();
#endif
    return LOS_OK;
}

static bool IsValidReply(const IpcContent *content)
{
    IpcMsg *requestMsg = (IpcMsg *)content->buffToFree;
    IpcMsg *replyMsg = content->outMsg;
    uint32_t reqDstTid = INVALID_ID;
    /* Check whether the reply matches the request */
    if ((requestMsg->type != MT_REQUEST)  ||
        (requestMsg->flag == LITEIPC_FLAG_ONEWAY) ||
        (replyMsg->timestamp != requestMsg->timestamp) ||
        (replyMsg->target.handle != requestMsg->taskID) ||
        (GetTid(requestMsg->target.handle, &reqDstTid) != 0) ||
        (g_ipc->taskCBIpc[reqDstTid].processID != g_processID)) {
        return false;
    }
    return true;
}

static int CheckPara(IpcContent *content, uint32_t *dstTid)
{
    int ret;
    IpcMsg *msg = content->outMsg;
    uint32_t flag = content->flag;
#if (USE_TIMESTAMP == YES)
    struct timespec realTime;
    clock_gettime(CLOCK_REALTIME, &realTime);
    uint64_t now = (uint64_t)realTime.tv_sec * 1000000000ULL + realTime.tv_nsec;
#endif
    if (((msg->dataSz > 0) && (msg->data == NULL)) ||
        ((msg->spObjNum > 0) && (msg->offsets == NULL)) ||
        (msg->dataSz > IPC_MSG_DATA_SZ_MAX) ||
        (msg->spObjNum > IPC_MSG_OBJECT_NUM_MAX) ||
        (msg->dataSz < msg->spObjNum * sizeof(SpecialObj))) {
        return -EINVAL;
    }
    switch (msg->type) {
        case MT_REQUEST:
            if (HasServiceAccess(msg->target.handle)) {
                ret = GetTid(msg->target.handle, dstTid);
                if (ret != LOS_OK) {
                    return -EINVAL;
                }
            } else {
                PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
                return -EACCES;
            }
#if (USE_TIMESTAMP == YES)
            msg->timestamp = now;
#endif
            break;
        case MT_REPLY:
        case MT_FAILED_REPLY:
            if ((flag & BUFF_FREE) != BUFF_FREE) {
                return -EINVAL;
            }
            if (!IsValidReply(content)) {
                return -EINVAL;
            }
#if (USE_TIMESTAMP == YES)
            if (now > msg->timestamp + LITEIPC_TIMEOUT_NS) {
#if (LOSCFG_KERNEL_TRACE == YES)
                IpcTrace(msg, WRITE_DROP, 0, msg->type);
                IpcBacktrace();
#endif
                PRINT_ERR("A timeout reply, request timestamp:%"PRIu64", now:%"PRIu64"\n", msg->timestamp, now);
                return -ETIME;
            }
#endif
            *dstTid = msg->target.handle;
            break;
        case MT_DEATH_NOTIFY:
            *dstTid = msg->target.handle;
#if (USE_TIMESTAMP == YES)
            msg->timestamp = now;
#endif
            break;
        default:
            PRINT_DEBUG("Unknow msg type:%d\n", msg->type);
            return -EINVAL;
    }

    if (IS_TASK_ALIVE(*dstTid) == false) {
        return -EINVAL;
    }
    return LOS_OK;
}

static int LiteIpcWrite(IpcContent *content)
{
    int ret;
    uint32_t dstTid;
    void *dstShm;

    IpcMsg *msg = content->outMsg;

    ret = CheckPara(content, &dstTid);
    if (ret != LOS_OK) {
        return ret;
    }

    uint64_t bufSz;
    ret = GetMsgSize(msg, &bufSz);
    if (ret != LOS_OK) {
        PRINT_DEBUG("%s, %d\n", __FUNCTION__, __LINE__);
        return ret;
    }
    if (IsCmsTask(dstTid) && (bufSz > g_ipc->maxMsgSize)) {
        PRINT_DEBUG("%s, %d\n", __FUNCTION__, __LINE__);
        return -EINVAL;
    }
    TaskCBIpc *dstTcb = &g_ipc->taskCBIpc[dstTid];
    dstShm = (dstTcb->processID == g_processID) ? 
        g_processShm : shmat(g_ipc->processCBIpc[dstTcb->processID].pool.svID, NULL, 0);
    if (dstShm == (void *)-1) {
        PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
        return -ENOMEM;
    }
    uintptr_t obuf;
    ret = LiteIpcNodeAlloc(dstShm, dstTcb->processID,
        bufSz + sizeof(IpcListNode) - sizeof(IpcMsg), &obuf);
    if (ret != LOS_OK) {
        PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
        if (dstShm != g_processShm) {
            shmdt(dstShm);
        }
        return -ENOMEM;
    }
    ret = CopyDataFromUser(dstShm, obuf, msg);
    if (ret != LOS_OK) {
        PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
        goto ERROR_COPY;
    }
    ret = HandleSpecialObjects(dstShm, obuf + sizeof(IpcListNode) +
        msg->dataSz + msg->spObjNum * sizeof(SpecialObj *), dstTid, obuf, false);
    if (ret != LOS_OK) {
        PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
        goto ERROR_COPY;
    }
    LIST_REAL_ADDRESS(dstShm, obuf)->nextNode = NULL;
    /* add data to list and wake up dest task */
    IpcListNode *dbuf = LIST_REAL_ADDRESS(g_ipc->processCBIpc[dstTcb->processID].pool.memBase, obuf);
    // need IS_TASK_ALIVE() check here?
    pthread_spin_lock(&dstTcb->messageListLock);
    if IS_LIST_EMPTY(dstTcb) {
        dstTcb->omessageListHead = obuf;
        dstTcb->messageListTail = dbuf;
        pthread_spin_unlock(&dstTcb->messageListLock);
    } else {
        LIST_REAL_ADDRESS(dstShm, dstTcb->omessageListHead)->nextNode = dbuf;
        dstTcb->omessageListHead = obuf;
        pthread_spin_unlock(&dstTcb->messageListLock);
    }
#if (LOSCFG_KERNEL_TRACE == YES)
    int semVal;
    sem_getvalue(&dstTcb->messageListSemaphore, &semVal);
    IpcTrace(&LIST_REAL_ADDRESS(dstShm, obuf)->msg, WRITE, semVal, LIST_REAL_ADDRESS(dstShm, obuf)->msg.type);
#endif
    if (dstShm != g_processShm) {
        shmdt(dstShm);
    }
    sem_post(&dstTcb->messageListSemaphore);
    sched_yield();
    return LOS_OK;
ERROR_COPY:
    LiteIpcNodeFree(dstShm, dstTcb->processID, obuf);
    if (dstShm != g_processShm) {
        shmdt(dstShm);
    }
    return ret;
}

static int CheckRecievedMsg(IpcListNode *node, IpcContent *content, TaskCBIpc *tcb)
{
    int ret = LOS_OK;
    if (node == NULL) {
        return -EINVAL;
    }
    switch (node->msg.type) {
        case MT_REQUEST:
            if ((content->flag & SEND) == SEND) {
                PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
                ret = -EINVAL;
            }
            break;
        case MT_FAILED_REPLY:
            ret = -ENOENT;
            /* fall-through */
        case MT_REPLY:
            if ((content->flag & SEND) != SEND) {
                PRINT_ERR("%s, %d\n", __FUNCTION__, __LINE__);
                ret = -EINVAL;
            }
#if (USE_TIMESTAMP == YES)
            if (node->msg.timestamp != content->outMsg->timestamp) {
                PRINT_ERR("Recieve a unmatch reply, drop it\n");
                ret = -EINVAL;
            }
#else
            if ((node->msg.code != content->outMsg->code) ||
                (node->msg.target.token != content->outMsg->target.token)) {
                PRINT_ERR("Recieve a unmatch reply, drop it\n");
                ret = -EINVAL;
            }
#endif
            break;
        case MT_DEATH_NOTIFY:
            break;
        default:
            PRINT_ERR("Unknow msg type:%d\n", node->msg.type);
            ret =  -EINVAL;
    }
    if (ret != LOS_OK) {
#if (LOSCFG_KERNEL_TRACE == YES)
        int semVal;
        sem_getvalue(&tcb->messageListSemaphore, &semVal);
        IpcTrace(&node->msg, READ_DROP, semVal, node->msg.type);
#endif
        (void)HandleSpecialObjects(g_processShm, 0, 0, OFFSET_ADDR(node), true);
        (void)LiteIpcNodeFree(g_processShm, g_processID, OFFSET_ADDR(node));
    } else {
#if (LOSCFG_KERNEL_TRACE == YES)
        int semVal;
        sem_getvalue(&tcb->messageListSemaphore, &semVal);
        IpcTrace(&node->msg, READ, semVal, node->msg.type);
#endif
    }
    return ret;
}

static int LiteIpcRead(IpcContent *content)
{
    int ret;
    IpcListNode *node = NULL;
    int syncFlag = (content->flag & SEND) && (content->flag & RECV);
    struct timespec timeout;

    TaskCBIpc *tcb = &g_ipc->taskCBIpc[GET_MYTASKID];
    do {
#if (LOSCFG_KERNEL_TRACE == YES)
        int semVal;
        sem_getvalue(&tcb->messageListSemaphore, &semVal);
        IpcTrace(NULL, TRY_READ, semVal, syncFlag ? MT_REPLY : MT_REQUEST);
#endif
        if (syncFlag) {
            clock_gettime(CLOCK_REALTIME, &timeout);
            timeout.tv_sec += LITEIPC_TIMEOUT_S;
            if ((sem_timedwait(&tcb->messageListSemaphore, &timeout) < 0) && (errno == ETIMEDOUT)) {
#if (LOSCFG_KERNEL_TRACE == YES)
                int semVal;
                sem_getvalue(&tcb->messageListSemaphore, &semVal);
                IpcTrace(NULL, READ_TIMEOUT, semVal, syncFlag ? MT_REPLY : MT_REQUEST);
#endif
                return -ETIME;
            }
        } else {
            sem_wait(&tcb->messageListSemaphore);
        }
        // No lock on empty check on premise that message lists are
        // thread-specific: only the thread attached to the service/message
        // list can read from the list.  Also assumes writes are complete
        // as soon as the tail pointer is set.
        if (!IS_LIST_EMPTY(tcb)) {
            node = tcb->messageListTail;
            pthread_spin_lock(&tcb->messageListLock);
            tcb->messageListTail = tcb->messageListTail->nextNode;
            pthread_spin_unlock(&tcb->messageListLock);
            ret = CheckRecievedMsg(node, content, tcb);
            if (ret == LOS_OK) {
                break;
            }
            if (ret == -ENOENT) { /* It means that we've received a failed reply */
                return ret;
            }
        }
    } while (1);
    node->msg.data = REAL_ADDRESS(g_processShm, node->msg.data);
    node->msg.offsets = REAL_ADDRESS(g_processShm, node->msg.offsets);
    content->inMsg = &node->msg;
    EnableIpcNodeFreeByUser(g_processID, (void *)node);
    return LOS_OK;
}

int LiteIpcMsgHandle(IpcContent *content)
{
    uint32_t myTaskID;
    int ret = LOS_OK;
    bool nodeNeedFreeFlag = false;
    uintptr_t onodeNeedFree;
    USER_ENTRY_PROCEDURE(myTaskID);

    if (IsCmsSet() == false) {
        PRINT_ERR("ServiceManager not set!\n");
        return -EINVAL;
    }

    if ((content->flag & BUFF_FREE) == BUFF_FREE) {
        ret = CheckUsedBuffer(content->buffToFree, &onodeNeedFree);
        if (ret != LOS_OK) {
            PRINT_ERR("CheckUsedBuffer failed:%d\n", ret);
            return ret;
        }
        nodeNeedFreeFlag = true;
    }

    if ((content->flag & SEND) == SEND) {
        if (content->outMsg == NULL) {
            PRINT_ERR("content->outmsg is null\n");
            ret = -EINVAL;
            goto BUFFER_FREE;
        }
        if ((content->outMsg->type < 0) || (content->outMsg->type >= MT_DEATH_NOTIFY)) {
            PRINT_ERR("LiteIpc unknown msg type:%d\n", content->outMsg->type);
            ret = -EINVAL;
            goto BUFFER_FREE;
        }
        ret = LiteIpcWrite(content);
        if (ret != LOS_OK) {
            PRINT_ERR("LiteIpcWrite failed\n");
            goto BUFFER_FREE;
        }
    }
BUFFER_FREE:
    if (nodeNeedFreeFlag) {
        int freeRet = LiteIpcNodeFree(g_processShm, g_processID, onodeNeedFree);
        ret = (freeRet == LOS_OK) ? ret : freeRet;
    }
    if (ret != LOS_OK) {
        return ret;
    }

    if ((content->flag & RECV) == RECV) {
        ret = LiteIpcRead(content);
        if (ret != LOS_OK) {
            PRINT_ERR("LiteIpcRead failed ERROR: %d\n", ret);
            return ret;
        }
    }
    return ret;
}

int HandleCmsCmd(CmsCmdContent *content)
{
    uint32_t myTaskID;
    int ret = LOS_OK;
    if (content == NULL) {
        return -EINVAL;
    }
    USER_ENTRY_PROCEDURE(myTaskID);

    if (IsCmsSet() == false) {
        PRINT_ERR("ServiceManager not set!\n");
        return -EINVAL;
    }

    if (IsCmsTask(myTaskID) == false) {
        return -EACCES;
    }
    switch (content->cmd) {
        case CMS_GEN_HANDLE:
            if (IS_TASK_ALIVE(content->taskID) == false) {
                return -EINVAL;
            }
            ret = GetServiceHandle(content->taskID, HANDLE_REGISTERED, &(content->serviceHandle));
            AddServiceAccess(g_ipc->serviceTaskID[0], content->serviceHandle);
            break;
        case CMS_REMOVE_HANDLE:
            if (content->serviceHandle >= MAX_SERVICE_NUM) {
                return -EINVAL;
            }
            RefreshServiceHandle(content->serviceHandle, -1);
            break;
        case CMS_ADD_ACCESS:
            if (IS_TASK_ALIVE(content->taskID) == false) {
                return -EINVAL;
            }
            return AddServiceAccess(content->taskID, content->serviceHandle);
        default:
            PRINT_DEBUG("Unknow cmd cmd:%d\n", content->cmd);
            return -EINVAL;
    }
    return ret;
}

int HandleGetVersion(IpcVersion *version)
{
    int ret = LOS_OK;
    if (version == NULL) {
        return -EINVAL;
    }

    version->driverVersion = DRIVER_VERSION;
    return ret; 
}

int SimulatorInitTask(uint32_t *taskID) {
    uint32_t myTaskID;
    USER_ENTRY_PROCEDURE(myTaskID);
    *taskID = myTaskID;

    return LOS_OK;
}

pid_t SimulatorGetSysPid(uint32_t processID) {
    int fd;
    pid_t pid;
    if (processID >= MAX_PROCESS_NUM) {
        return 0;
    }
    if (g_ipc != NULL) {
        return g_ipc->processCBIpc[processID].pid;
    }
    fd = shm_open("/liteipc_simulator", O_RDWR, 0666);
    if (fd < 0) {
        return 0;
    }
    lseek(fd, offsetof(SimulatorControlData, processCBIpc[processID].pid), SEEK_SET);
    if (read(fd, &pid, sizeof(pid_t)) != sizeof(pid_t)) {
        pid = 0;
    }
    close(fd);
    return pid;
}
