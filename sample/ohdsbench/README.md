### ohdsbench

A simple benchmark program for the distributed scheduler which calls a service that does nothing to measure service invocation overhead.  Add an entry for service "ohds_noop" with feature "noop" to security/permission_lite/ipc_auth/include/policy_preset.h (located in the product_adapter_dir directory specified by the product's config.json) before building.  Run noop_service in the background then run ohdsbench.  Try running two or more instances of ohdsbench at the same time and see what happens.
