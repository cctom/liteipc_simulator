#include "ohds_noop.h"
#include "iproxy_client.h"
#include "registry.h"
#include <stdlib.h>

struct NoopClient {
    INHERIT_IUNKNOWNENTRY(struct OHDSNoop);
};

static void noop(IUnknown *this) {
    ((IClientProxy *)this)->Invoke((IClientProxy *)this, 0, NULL, NULL, NULL);
}

static void *creator(const char *service, const char *feature, uint32 size) {
    void *buf = calloc(1, size + sizeof(struct NoopClient));
    struct NoopClient * const client = (struct NoopClient *)((uintptr_t)buf + size);
    client->ver = CLIENT_PROXY_VER | DEFAULT_VERSION;
    client->ref = 1;
    client->iUnknown.QueryInterface = IUNKNOWN_QueryInterface;
    client->iUnknown.noop = noop;
    return buf;
}

static void destroyer(const char *service, const char *feature, void *iproxy) {
    free(iproxy);
}

static __attribute__((constructor)) void init_client(void) {
    SAMGR_RegisterFactory(NOOP_SVC_NAME, NOOP_FEAT_NAME, creator, destroyer);
}
