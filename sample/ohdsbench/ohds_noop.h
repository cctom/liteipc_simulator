#ifndef OHDS_NOOP_H
#define OHDS_NOOP_H

#include "iproxy_client.h"

#define NOOP_SVC_NAME  "ohds_noop"
#define NOOP_FEAT_NAME "noop"

struct OHDSNoop {
    INHERIT_CLIENT_IPROXY;
    void (*noop)(IUnknown *this);
};

#endif
