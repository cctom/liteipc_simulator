#include "ohds_noop.h"
#include "samgr_lite.h"
#include <stdio.h>
#include <time.h>

#define REPS 1000

int main(void) {
    IUnknown *iUnknown;
    struct OHDSNoop *noop;
    struct timespec startTime;
    struct timespec endTime;
    int i;
    iUnknown = SAMGR_GetInstance()->GetFeatureApi(NOOP_SVC_NAME, NOOP_FEAT_NAME);
    if (iUnknown == NULL) {
        printf("noop API not found.\n");
        return -1;
    }
    if (iUnknown->QueryInterface(iUnknown, CLIENT_PROXY_VER, (void **)&noop) != EC_SUCCESS) {
        printf("Unable to get API client.\n");
        return -1;
    }
    clock_gettime(CLOCK_REALTIME, &startTime);
    for(i = 0; i < REPS; i++) {
        noop->noop((IUnknown *)noop);
    }
    clock_gettime(CLOCK_REALTIME, &endTime);
    printf("%Lf useconds per noop.\n", (long double)((endTime.tv_sec - startTime.tv_sec) * 1000000000ULL + endTime.tv_nsec - startTime.tv_nsec) / 1000 / REPS);
    noop->Release((IUnknown *)noop);
    return 0;
}
