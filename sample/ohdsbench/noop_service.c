#include "ohds_noop.h"
#include "samgr_lite.h"
#include "iproxy_server.h"
#include <unistd.h>
#include <stdio.h>

static const char *GetServiceName(Service *service) { return NOOP_SVC_NAME; }
static BOOL Initialize(Service *service, Identity identity) { return TRUE; }
static BOOL MessageHandle(Service *service, Request *request) { return TRUE; }
static TaskConfig GetTaskConfig(Service *service) { return (TaskConfig){ LEVEL_LOW, PRI_NORMAL, 256, 256, SHARED_TASK }; }
static Service noopService = {
    .GetName = GetServiceName,
    .Initialize = Initialize,
    .MessageHandle = MessageHandle,
    .GetTaskConfig = GetTaskConfig
};

static const char *GetFeatureName(Feature *feature) { return NOOP_FEAT_NAME; }
static void OnInitialize(Feature *feature, Service *parent, Identity identity) { }
static void OnStop(Feature *feature, Identity identity) { }
static BOOL OnMessage(Feature *feature, Request *request) { return TRUE; }
static Feature noopFeature = {
    .GetName = GetFeatureName,
    .OnInitialize = OnInitialize,
    .OnStop = OnStop,
    .OnMessage = OnMessage
};

static int QueryInterface(IUnknown *iUnknown, int version, void **target) {
    if (!(version & SERVER_IMPL_PROXY_VER)) {
        return -1;
    }
    *target = iUnknown;
    return EC_SUCCESS;
}
static int AddRef(IUnknown *iUnknown) { return 1; }
static int Release(IUnknown *iUnknown) { return 1; }
static int32 Invoke(IServerProxy *iProxy, int funcId, void *origin, IpcIo *req, IpcIo *reply) { return EC_SUCCESS; }
static IServerProxy noop = {
    .QueryInterface = QueryInterface,
    .AddRef = AddRef,
    .Release = Release,
    .Invoke = Invoke
};

int main(void) {
    if (SAMGR_GetInstance()->RegisterService(&noopService) == FALSE) {
        printf("Service registration failed.\n");
        return -1;
    }
    if (SAMGR_GetInstance()->RegisterFeature(NOOP_SVC_NAME, &noopFeature) == FALSE) {
        printf("Feature registration failed.\n");
        return -1;
    }
    if (SAMGR_GetInstance()->RegisterFeatureApi(NOOP_SVC_NAME, NOOP_FEAT_NAME, (IUnknown *)&noop) == FALSE) {
        printf("API registration failed.\n");
        return -1;
    }
    SAMGR_Bootstrap();
    pause();
    return 0;
}
