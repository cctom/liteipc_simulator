## License
The LiteIPC Simulator is released under the same conditions as all code in the OpenHarmony project.  In particular, the terms of the [OpenHarmony CLA](https://gitee.com/openharmony/community/blob/master/cla) apply to the LiteIPC Simulator project.

## Overview
LiteIPC Simulator is an implementation of OpenHarmony's LiteIPC (see //docs/lite/en/readme/liteipc_driver.md for more) using POSIX threads, shared memory and semaphores and the Linux implementation of System V shared memory.  It was intended to enable easier use/development of more mini and small systems' OpenHarmony components on Linux.  With OpenHarmony's unification under the Single IPC framework\[[communication_ipc #I52D5F](https://gitee.com/openharmony/communication_ipc/issues/I52D5F)\], the framework's Binder based IPC can be used for development on Linux instead.  I will leave the simulator repository for those interested in playing with LiteIPC on their Linux box or as a starting point for anyone who doesn't want to build a kernel with Binder, but will not try to keep it up to date.

## Differences/Limitations
The most important differences between LiteIPC and the simulator are the memory management method and automatic cleanup of task IPC information.  The simulator uses a much simpler memory management method, which more easily results in out of memory conditions if a task holds onto a message instead of processing and then immediately freeing it upon receipt.  The simulator also can't detect threads which have ended when their process continues running.  This can be particularly problematic when combined with the limitations of the memory model if a task receives a message after ending.  If something stops working with a lot of out of memory errors, this is probably the cause.

The simulator doesn't support sending file descriptors in OBJ_FD special objects, which is needed by the DSoftBus' TCP direct channel type at least.

## Use
The simulator was designed to transparently replace OpenHarmony's liteipc_adapter.  The simplest way to use the simulator was to change the //foundation/communication/ipc_lite:liteipc_adapter target to point at the simulator's liteipc group target.

eg. change the liteipc_adapter target for linux builds to something like
```
group("liteipc_adapter") {
    public_deps = [ "//third_party/liteipc_simulator:liteipc" ]
}
```
if the simulator is located in the third_party/liteipc_simulator directory under the OpenHarmony project directory.

Run liteipc_simulator_init before running OpenHarmony programs using LiteIPC.

## Directory structure
The `simulator` directory contains the LiteIPC simulator source code.  
- The liteipc_simulator_init program performs the role of LiteIPC's OsLiteIpcInit function.  It creates and initializes a POSIX shared memory object named liteipc_simulator which contains all the control information for processes using the simulator to communicate with each other.  As with OsLiteIpcInit it should only be called once.  

The `adapter` directory contains the OpenHarmony liteipc_adapter modified to work with the simulator.  
The `sample` directory contains a sample program which uses LiteIPC.  
The `test` directory contains simple test cases.  
The `utils` directory contains a utility program for simulator development.  
- liteipc_simulator_remove removes the shared memory created by liteipc_simulator_init.  It doesn't perform error checking, and expects that no processes are still attached to the simulator.
